@extends('frontend.layouts.app')

@section('title')
    {{app_name()}}
@endsection

@section('content')

    <div class="home">
        <section class="home__banner position-relative">
            <div class="home__banner--img d-flex m-auto">
                <img class="w-100" src="{{asset('img/banner-home.png')}}" alt="banner">
            </div>
            <div class="home__banner--description d-flex flex-column align-items-center position-absolute top-50 start-50 translate-middle w-100">
                <h5 class="fw-bold wt-font-antonio text-white">WE TRIP ASIA</h5>
                <p class="wt-font-merriweather wt-fw-300 fst-italic text-white">Your best local DMC in Vietnam, Cambodia, Laos & Myanmar</p>
                <button type="button" class="wt-font-merriweather wt-fw-300 text-white px-5 py-2 wt-bg-blue">
                    Send your inquiry
                </button>
            </div>
        </section>
        <section class="home__why-us container p-md-5 p-3">
            <div class="home__why-us--title d-flex justify-content-center">
                <h4 class="wt-font-antonio wt-fw-400 wt-color-gray wt-fz-48 wt-line-gradient position-relative py-4">Why Us</h4>
            </div>
            <div class="home__why-us--description d-flex flex-column align-items-center wt-bg-blue-transparent mt-5 px-md-5 px-3 py-4">
                <i class="fa-solid fa-quote-left wt-color-blue-2 wt-fz-26 mb-3 opacity-25"></i>
                <p class="wt-color-blue-3 wt-font-merriweather fw-bold wt-fz-24 text-center">As one stop travel service provider, we cater wide range of services for both leisure and MICE from single
                                                                                             services
                                                                                             like airport transfer or hotel booking to a door-to-door packages</p>
            </div>
            <div class="home__why-us--explore mt-5">
                <div class="title d-flex flex-column justify-content-center align-items-center mb-4">
                    <h4 class="wt-color-green">Explore. Save. Share</h4>
                    <p class="text-uppercase wt-fz-18 wt-fw-400 my-3">Plan your <span class="wt-font-merriweather text-lowercase fst-italic">dream</span> vacation.</p>
                </div>
                <div class="description">
                    <p class="pb-3">
                        As one stop travel service provider, we cater wide range of services for both leisure and MICE from single services like airport transfer or hotel booking to a door-to-door
                        packages. With years of experience in this industry, wide knowledges and insights, enthusiasm, and vision of the insiders, our team always go beyond the usual to bring our
                        guests an authentic vacation with unique experience and best-in-service quality.
                    </p>
                    <p class="pb-3">
                        Strong relationships and partnerships with local suppliers together with multiple branch offices across the Indochina countries, we are proud of the capacity to support our
                        guests promptly solving any issue pop-up while traveling or well meeting all last minutes/at once request risen on the spots.
                    </p>
                    <p class="pb-3">
                        Successfully designed countless vacations for individuals, families, groups, and corporates, we are thrilled to see their smiles and satisfactions bidding farewell our
                        destinations to home sweet home. Though the world is so redundant information and competitions, we believe that the sincerity and professional always touches the heart.
                    </p>
                    <div class="pb-md-5 pb-4 w-100 position-relative wt-line-gradient">
                        <img class="mt-4 w-100" src="{{asset('img/div.vegas-slide-inner.png')}}" alt="div.vegas-slide-inner">
                    </div>
                </div>
            </div>
        </section>
        <section class="home__story container p-md-5 p-3">
            <div class="home__story--title mb-4">
                <h4 class="wt-font-merriweather wt-fw-400 wt-color-gray wt-fz-48 text-center mb-5">Our Story</h4>
            </div>
            <div class="home__story--content">
                <div class="row">
                    <div class="col-lg-4 col-sm-6 col-12 p-0 mb-lg-0 mb-3">
                        <img class="w-100" src="{{asset('img/image 1.png')}}" alt="image 1">
                    </div>
                    <div class="col-lg-4 col-sm-6 col-12 p-0 mb-lg-0 mb-3 d-flex flex-column justify-content-center align-items-center">
                        <h6 class="fw-bold wt-font-merriweather fst-italic wt-color-black text-center wt-fz-20">Sapa's Mountain Charms</h6>
                        <p class="wt-color-gray-2 text-center wt-fz-16 px-4 pt-2">Our Vietnam journey began in the mesmerizing heights of Sapa, where terraced rice fields and resilient hill tribes
                                                                                  painted an enchanting picture of northern Vietnam's wilderness.</p>
                    </div>
                    <div class="col-lg-4 col-sm-6 col-12 p-0 mb-lg-0 mb-3">
                        <img class="w-100" src="{{asset('img/image 2.png')}}" alt="image 1">
                    </div>
                    <div class="col-lg-4 col-sm-6 col-12 p-0 mb-lg-0 mb-3 d-flex flex-column justify-content-center align-items-center">
                        <h6 class="fw-bold wt-font-merriweather fst-italic wt-color-black text-center wt-fz-20">Cultural Gems of Hanoi and Hoi An</h6>
                        <p class="wt-color-gray-2 text-center wt-fz-16 px-4 pt-2">In Hanoi's bustling streets and Hoi An's quaint alleys, we immersed ourselves in Vietnam's cultural tapestry,
                                                                                  indulging in traditional flavors and timeless architectural wonders.</p>
                    </div>
                    <div class="col-lg-4 col-sm-6 col-12 p-0 mb-lg-0 mb-3">
                        <img class="w-100" src="{{asset('img/image 3.png')}}" alt="image 1">
                    </div>
                    <div class="col-lg-4 col-sm-6 col-12 p-0 mb-lg-0 mb-3 d-flex flex-column justify-content-center align-items-center">
                        <h6 class="fw-bold wt-font-merriweather fst-italic wt-color-black text-center wt-fz-20">Natural Marvels: Ha Long Bay and Mekong Delta</h6>
                        <p class="wt-color-gray-2 text-center wt-fz-16 px-4 pt-2">From the mystical karsts of Ha Long Bay to the tranquil waterways of the Mekong Delta, we reveled in Vietnam's natural
                                                                                  splendors, fostering a deep admiration for the country's diverse landscapes and warm-hearted people.</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="home__favorite container p-md-5 p-3">
            <div class="d-flex flex-column justify-content-center align-items-center mb-5">
                <img src="{{asset('img/map.png')}}" alt="">
                <span class="wt-fz-20 mt-3 my-4">Find Your</span>
                <h5 class="wt-font-merriweather wt-fz-40 wt-color-blue">Favorite Destination</h5>
            </div>
            <div class="home__favorite--slider position-relative">
                <div class="slide">
                    <div class="slider-item position-relative">
                        <img class="w-100 h-100" src="{{asset('img/viet-nam.png')}}" alt="">
                        <div class="detail d-flex flex-column justify-content-center align-items-center position-absolute start-50 translate-middle-x px-md-5 px-4 py-3">
                            <h5 class="wt-font-merriweather wt-fz-20 fw-bold text-white mb-2">VIETNAM</h5>
                            <p class="wt-color-blue-4 wt-cursor">View more</p>
                        </div>
                    </div>
                    <div class="slider-item position-relative">
                        <img class="w-100 h-100" src="{{asset('img/laos.png')}}" alt="">
                        <div class="detail d-flex flex-column justify-content-center align-items-center position-absolute start-50 translate-middle-x px-md-5 px-4 py-3">
                            <h5 class="wt-font-merriweather wt-fz-20 fw-bold text-white mb-2">LAOS</h5>
                            <p class="wt-color-blue-4 wt-cursor">View more</p>
                        </div>
                    </div>
                    <div class="slider-item position-relative">
                        <img class="w-100 h-100" src="{{asset('img/campuchia.png')}}" alt="">
                        <div class="detail d-flex flex-column justify-content-center align-items-center position-absolute start-50 translate-middle-x px-md-5 px-4 py-3">
                            <h5 class="wt-font-merriweather wt-fz-20 fw-bold text-white mb-2">CAMPUCHIA</h5>
                            <p class="wt-color-blue-4 wt-cursor">View more</p>
                        </div>
                    </div>
                </div>
                <button class="prev-btn position-absolute bottom-50">
                    <i class="fa-solid fa-angle-left"></i>
                </button>
                <button class="next-btn position-absolute bottom-50">
                    <i class="fa-solid fa-angle-right"></i>
                </button>
                <div class="mt-5 position-relative mx-5 wt-line-gradient">
                </div>
            </div>
        </section>
        <section class="home__selling container p-md-5 p-3">
            <div class="mb-5">
                <h5 class="wt-font-merriweather wt-fz-40 wt-color-blue text-center">Best Selling Tours 2023</h5>
                <div style="width: 200px" class="position-relative d-flex mt-4 m-auto wt-line-gradient">
                </div>
            </div>
            <div class="home__selling--content">
                <div class="row">
                    <div class="col-md-6 col-12 mb-4">
                        <div class="position-relative">
                            <div>
                                <img src="{{asset('img/capital-hanoi.png')}}" alt="">
                            </div>
                            <div class="price d-flex justify-content-between align-items-center p-md-4 p-3 position-absolute bottom-0 w-100">
                                <div class="d-flex flex-column justify-content-evenly text-white">
                                    <div class="d-flex align-items-center mb-3">
                                        <img src="{{asset('img/schedule-icon.png')}}" alt="schedule-icon">
                                        <span class="text-uppercase pl-2">Year Around</span>
                                    </div>
                                    <div class="d-flex align-items-center">
                                        <img src="{{asset('img/clock.png')}}" alt="clock">
                                        <span class="text-uppercase pl-2">10 DAYS</span>
                                    </div>
                                </div>
                                <div class="d-flex text-white align-items-center">
                                    <p class="text-end wt-fw-300">$ <br> US</p>
                                    <p class="wt-fz-40 wt-fw-600 pl-2">299</p>
                                </div>
                            </div>
                            <div class="position-absolute top-50 start-50 translate-middle w-100 d-flex justify-content-center">
                                <div class="details px-5 py-3">
                                    <h5 class="wt-font-antonio wt-fz-36 text-center text-white mb-2">Impressive Vietnam</h5>
                                    <p class="text-center text-white fst-italic wt-fw-300 wt-fz-20">Hanoi, Ha Long, Hoi An, <br> Hue, HCMC</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-12 mb-4">
                        <div class="position-relative">
                            <div>
                                <img src="{{asset('img/capital-hanoi.png')}}" alt="">
                            </div>
                            <div class="price d-flex justify-content-between align-items-center p-md-4 p-3 position-absolute bottom-0 w-100">
                                <div class="d-flex flex-column justify-content-evenly text-white">
                                    <div class="d-flex align-items-center mb-3">
                                        <img src="{{asset('img/schedule-icon.png')}}" alt="schedule-icon">
                                        <span class="text-uppercase pl-2">Year Around</span>
                                    </div>
                                    <div class="d-flex align-items-center">
                                        <img src="{{asset('img/clock.png')}}" alt="clock">
                                        <span class="text-uppercase pl-2">10 DAYS</span>
                                    </div>
                                </div>
                                <div class="d-flex text-white align-items-center">
                                    <p class="text-end wt-fw-300">$ <br> US</p>
                                    <p class="wt-fz-40 wt-fw-600 pl-2">299</p>
                                </div>
                            </div>
                            <div class="position-absolute top-50 start-50 translate-middle w-100 d-flex justify-content-center">
                                <div class="details px-5 py-3">
                                    <h5 class="wt-font-antonio wt-fz-36 text-center text-white mb-2">Ninh Binh</h5>
                                    <p class="text-center text-white fst-italic wt-fw-300 wt-fz-20">Hanoi, Ha Long, Hoi An, <br> Hue, HCMC</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-12 mb-4">
                        <div class="position-relative">
                            <div>
                                <img src="{{asset('img/capital-hanoi.png')}}" alt="">
                            </div>
                            <div class="price d-flex justify-content-between align-items-center p-md-4 p-3 position-absolute bottom-0 w-100">
                                <div class="d-flex flex-column justify-content-evenly text-white">
                                    <div class="d-flex align-items-center mb-3">
                                        <img src="{{asset('img/schedule-icon.png')}}" alt="schedule-icon">
                                        <span class="text-uppercase pl-2">Year Around</span>
                                    </div>
                                    <div class="d-flex align-items-center">
                                        <img src="{{asset('img/clock.png')}}" alt="clock">
                                        <span class="text-uppercase pl-2">10 DAYS</span>
                                    </div>
                                </div>
                                <div class="d-flex text-white align-items-center">
                                    <p class="text-end wt-fw-300">$ <br> US</p>
                                    <p class="wt-fz-40 wt-fw-600 pl-2">299</p>
                                </div>
                            </div>
                            <div class="position-absolute top-50 start-50 translate-middle w-100 d-flex justify-content-center">
                                <div class="details px-5 py-3">
                                    <h5 class="wt-font-antonio wt-fz-36 text-center text-white mb-2">Ha Long Bay</h5>
                                    <p class="text-center text-white fst-italic wt-fw-300 wt-fz-20">Hanoi, Ha Long, Hoi An, <br> Hue, HCMC</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-12 mb-4">
                        <div class="position-relative">
                            <div>
                                <img src="{{asset('img/capital-hanoi.png')}}" alt="">
                            </div>
                            <div class="price d-flex justify-content-between align-items-center p-md-4 p-3 position-absolute bottom-0 w-100">
                                <div class="d-flex flex-column justify-content-evenly text-white">
                                    <div class="d-flex align-items-center mb-3">
                                        <img src="{{asset('img/schedule-icon.png')}}" alt="schedule-icon">
                                        <span class="text-uppercase pl-2">Year Around</span>
                                    </div>
                                    <div class="d-flex align-items-center">
                                        <img src="{{asset('img/clock.png')}}" alt="clock">
                                        <span class="text-uppercase pl-2">10 DAYS</span>
                                    </div>
                                </div>
                                <div class="d-flex text-white align-items-center">
                                    <p class="text-end wt-fw-300">$ <br> US</p>
                                    <p class="wt-fz-40 wt-fw-600 pl-2">299</p>
                                </div>
                            </div>
                            <div class="position-absolute top-50 start-50 translate-middle w-100 d-flex justify-content-center">
                                <div class="details px-5 py-3">
                                    <h5 class="wt-font-antonio wt-fz-36 text-center text-white mb-2">Hoi An</h5>
                                    <p class="text-center text-white fst-italic wt-fw-300 wt-fz-20">Hanoi, Ha Long, Hoi An, <br> Hue, HCMC</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="home__affiliate container p-md-5 p-3">
            <div class="home__affiliate--saying">
                <div class="text-uppercase text-center fw-bold wt-fz-14 wt-color-blue-5 mb-5 pb-3">What people are saying...</div>
                <div class="slider">
                    <div class="slider-item">
                        <div class="stars d-flex justify-content-center mb-4">
                            <i class="fa-solid fa-star pr-2"></i>
                            <i class="fa-solid fa-star pr-2"></i>
                            <i class="fa-solid fa-star pr-2"></i>
                            <i class="fa-solid fa-star pr-2"></i>
                            <i class="fa-solid fa-star pr-2"></i>
                        </div>
                        <div class="details mb-4">
                            <p class="wt-font-merriweather fst-italic wt-fz-24 text-center mb-4 wt-fw-600">“The best so far prototyping tool out there, very easy to use for a non coder, <br> highly recommended.”</p>
                            <h6 class="text-center text-uppercase fw-bold wt-fz-14 text-black mb-2">Randy & Victoria</h6>
                            <p class="text-center text-gray-500 fw-bold wt-fz-12">Hanoi - VN</p>
                        </div>
                    </div>
                    <div class="slider-item">
                        <div class="stars d-flex justify-content-center mb-4">
                            <i class="fa-solid fa-star pr-2"></i>
                            <i class="fa-solid fa-star pr-2"></i>
                            <i class="fa-solid fa-star pr-2"></i>
                            <i class="fa-solid fa-star pr-2"></i>
                            <i class="fa-solid fa-star pr-2"></i>
                        </div>
                        <div class="details mb-4">
                            <p class="wt-font-merriweather fst-italic wt-fz-24 text-center mb-4 wt-fw-600">“The best so far prototyping tool out there, very easy to use for a non coder, <br> highly recommended.”</p>
                            <h6 class="text-center text-uppercase fw-bold wt-fz-14 text-black mb-2">Randy & Victoria</h6>
                            <p class="text-center text-gray-500 fw-bold wt-fz-12">Hanoi - VN</p>
                        </div>
                    </div>
                    <div class="slider-item">
                        <div class="stars d-flex justify-content-center mb-4">
                            <i class="fa-solid fa-star pr-2"></i>
                            <i class="fa-solid fa-star pr-2"></i>
                            <i class="fa-solid fa-star pr-2"></i>
                            <i class="fa-solid fa-star pr-2"></i>
                            <i class="fa-solid fa-star pr-2"></i>
                        </div>
                        <div class="details mb-4">
                            <p class="wt-font-merriweather fst-italic wt-fz-24 text-center mb-4 wt-fw-600">“The best so far prototyping tool out there, very easy to use for a non coder, <br> highly recommended.”</p>
                            <h6 class="text-center text-uppercase fw-bold wt-fz-14 text-black mb-2">Randy & Victoria</h6>
                            <p class="text-center text-gray-500 fw-bold wt-fz-12">Hanoi - VN</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="home__affiliate--our mt-5">
                <h5 class="wt-font-merriweather wt-fw-400 wt-fz-30 wt-color-gray text-center mb-4 pb-2">
                    Our Affiliate
                </h5>
                <div class="type row">
                    <div class="col-lg-3 col-sm-6 col-12 d-flex justify-content-center wt-cursor">
                        <img class="my-3" src="{{asset('img/image 17.png')}}" alt="image 17">
                    </div>
                    <div class="col-lg-3 col-sm-6 col-12 d-flex justify-content-center wt-cursor">
                        <img class="my-3" src="{{asset('img/image 18.png')}}" alt="image 18">
                    </div>
                    <div class="col-lg-3 col-sm-6 col-12 d-flex justify-content-center wt-cursor">
                        <img class="my-3" src="{{asset('img/image 19.png')}}" alt="image 19">
                    </div>
                    <div class="col-lg-3 col-sm-6 col-12 d-flex justify-content-center wt-cursor">
                        <img class="my-3" src="{{asset('img/image 20.png')}}" alt="image 20">
                    </div>
                </div>
            </div>
        </section>
        <section class="home__contact container p-md-5 p-3">
            <div class="row">
                <div class="col-sm-10 col-9">
                    <div class="row">
                        <div class="col-lg-6 col-12 mb-4">
                            <h5 class="mb-5 pb-4 wt-font-merriweather fw-bold wt-fz-32">Let’s Get in Touch</h5>
                            <div class="row mb-4">
                                <div class="col-lg-12 col-6 mb-3">
                                    <h6 class="text-uppercase wt-fz-14 wt-fw-500 wt-color-gray mb-2">Australia</h6>
                                    <p class="wt-fz-14 wt-fw-400 wt-color-gray mb-2">2 Pinewood Ave, Kardinya, WA 6163, Australia</p>
                                    <p class="wt-fz-14 wt-fw-400 wt-color-gray mb-2">079 5567 2982</p>
                                </div>
                                <div class="col-lg-12 col-6 mb-3">
                                    <h6 class="text-uppercase wt-fz-14 wt-fw-500 wt-color-gray mb-2">Vietnam</h6>
                                    <p class="wt-fz-14 wt-fw-400 wt-color-gray mb-2">207 Cau Giay, Hanoi, Vietnam</p>
                                    <p class="wt-fz-14 wt-fw-400 wt-color-gray mb-2">(+84) 941 960 024  - (+84) 941 960 124</p>
                                </div>
                            </div>
                            <div class="d-flex">
                                <i class="fa-regular fa-envelope"></i>
                                <p class="pl-3 wt-fz-16 wt-fw-400 wt-color-black">contact@wetrip.vn</p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-12 mb-4">
                        <div class="form row">
                            <div class="col-md-6 col-12 mb-4">
                                <input class="w-100" type="text" placeholder="Your name">
                            </div>
                            <div class="col-md-6 col-12 mb-4">
                                <input class="w-100" type="text" placeholder="Email address">
                            </div>
                            <div class="col-12 mb-4">
                                <input class="w-100" type="text" placeholder="Contact phone">
                            </div>
                            <div class="col-12 mb-5">
                                <textarea class="w-100" type="text" placeholder="Your comment"></textarea>
                            </div>
                            <div class="col-12 mb-4">
                                <button type="button" class="wt-bg-blue d-flex align-items-center text-white"><p class="wt-fz-14 wt-fw-400 p-3">SUBMIT MESSAGE</p> <i class="fa-solid fa-arrow-right-long p-3"></i></button>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-sm-2 col-3 d-flex justify-content-end">
                    <div class="type d-flex flex-column px-4">
                        <img class="py-4" src="{{asset('img/Group 143.png')}}" alt="Group">
                        <img class="py-4" src="{{asset('img/Group 71.png')}}" alt="Group">
                        <img class="py-4" src="{{asset('img/icon.png')}}" alt="icon">
                        <img class="py-4" src="{{asset('img/facebook icon.png')}}" alt="facebook">
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
