<?php

namespace Modules\Cambodium\Console\Commands;

use Illuminate\Console\Command;

class CambodiumCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:CambodiumCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cambodium Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        return Command::SUCCESS;
    }
}
