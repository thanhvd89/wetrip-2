<?php

namespace Modules\Cambodium\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cambodium extends BaseModel
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'cambodia';

    /**
     * Create a new factory instance for the model.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    protected static function newFactory()
    {
        return \Modules\Cambodium\database\factories\CambodiumFactory::new();
    }
}
