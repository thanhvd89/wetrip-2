<div class="header container p-3 position-fixed start-50 translate-middle-x">
    <div class="d-lg-flex justify-content-between d-none">
        <div class="header__logo">
            <img class="w-100 wt-cursor" src="{{asset('img/Frame 483125.png')}}" alt="logo">
        </div>
        <div class="header__menu d-flex align-items-center">
            <ul class="d-flex m-0 p-0 align-items-center">
                <li class="wt-color-blue">
                    <a class="text-uppercase wt-cursor wt-fz-18 wt-fw-400 wt-color-blue" href="">Destination</a>
                </li>
                <li class="wt-color-blue">
                    <a class="text-uppercase wt-cursor wt-fz-18 wt-fw-400 wt-color-blue" href="">Our Services</a>
                </li>
                <li class="wt-color-blue">
                    <a class="text-uppercase wt-cursor wt-fz-18 wt-fw-400 wt-color-blue" href="">Typical tour</a>
                </li>
                <li class="wt-color-blue">
                    <a class="text-uppercase wt-cursor wt-fz-18 wt-fw-400 wt-color-blue" href="">Gallery</a>
                </li>
                <li class="wt-color-blue">
                    <a class="text-uppercase wt-cursor wt-fz-18 wt-fw-400 wt-color-blue" href="">Contact US</a>
                </li>
            </ul>
        </div>
        <div class="header__search d-flex align-items-center wt-cursor">
            <i class="fa-solid fa-magnifying-glass wt-color-blue wt-fz-22"></i>
        </div>
    </div>

    <div class="mobile d-flex justify-content-between d-lg-none position-relative">
        <div class="menu d-flex align-items-center">
            <div class="hamburger wt-cursor">
                <i class="fa-solid fa-bars wt-color-blue wt-fz-24"></i>
            </div>
            <ul class="d-none position-absolute bg-white w-100 p-3">
                <li class="px-3 py-2">
                    <a class="wt-cursor wt-fz-14 wt-fw-600 wt-color-gray" href="">Destination</a>
                </li>
                <li class="px-3 py-2">
                    <a class="wt-cursor wt-fz-14 wt-fw-600 wt-color-gray" href="">Our Services</a>
                </li>
                <li class="px-3 py-2">
                    <a class="wt-cursor wt-fz-14 wt-fw-600 wt-color-gray" href="">Typical tour</a>
                </li>
                <li class="px-3 py-2">
                    <a class="wt-cursor wt-fz-14 wt-fw-600 wt-color-gray" href="">Gallery</a>
                </li>
                <li class="px-3 py-2">
                    <a class="wt-cursor wt-fz-14 wt-fw-600 wt-color-gray" href="">Contact US</a>
                </li>
            </ul>
        </div>
        <div class="logo">
            <img class="w-100 wt-cursor" src="{{asset('img/Frame 483125.png')}}" alt="logo">
        </div>
        <div class="search d-flex align-items-center wt-cursor">
            <i class="fa-solid fa-magnifying-glass wt-color-blue wt-fz-22"></i>
        </div>
    </div>
</div>
