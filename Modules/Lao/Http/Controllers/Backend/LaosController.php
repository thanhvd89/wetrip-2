<?php

namespace Modules\Lao\Http\Controllers\Backend;

use App\Authorizable;
use App\Http\Controllers\Backend\BackendBaseController;

class LaosController extends BackendBaseController
{
    use Authorizable;

    public function __construct()
    {
        // Page Title
        $this->module_title = 'Laos';

        // module name
        $this->module_name = 'laos';

        // directory path of the module
        $this->module_path = 'lao::backend';

        // module icon
        $this->module_icon = 'fa-regular fa-sun';

        // module model name, path
        $this->module_model = "Modules\Lao\Models\Lao";
    }

}
