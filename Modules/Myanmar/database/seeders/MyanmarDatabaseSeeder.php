<?php

namespace Modules\Myanmar\database\seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Tag\Models\Myanmar;

class MyanmarDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Disable foreign key checks!
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        /*
         * Myanmars Seed
         * ------------------
         */

        // DB::table('myanmars')->truncate();
        // echo "Truncate: myanmars \n";

        Myanmar::factory()->count(20)->create();
        $rows = Myanmar::all();
        echo " Insert: myanmars \n\n";

        // Enable foreign key checks!
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
